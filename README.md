# SAS Timing Test Doubles

[![Image](https://gitlab.com/sas_public/time-test-doubles/-/badges/release.svg)](https://gitlab.com/sas_public/time-test-doubles/-/releases)[![Image](https://www.vipm.io/package/system_automation_solutions_llc_lib_sas_timing_test_doubles/badge.svg?metric=installs)](https://www.vipm.io/package/system_automation_solutions_llc_lib_sas_timing_test_doubles/) [![Image](https://www.vipm.io/package/system_automation_solutions_llc_lib_sas_timing_test_doubles/badge.svg?metric=stars)](https://www.vipm.io/package/system_automation_solutions_llc_lib_sas_timing_test_doubles/)

[TOC]

## Getting Started

This is a toolkit for overriding native LV timing VIs for testing purposes. Timing VIs introduce randomness into your tests, because you can't predict ahead of time when the tests will be run.

You use the VIs from this pallette instead of the Native VIs. Behind the scenes the native timing functions are called. However you can override the Native Timing class to inject your own timing VIs.

### Creating an override

It starts by creating a child of the Native Timing Class. Then just override the native methods with whatever you want to replace them with.

### Using the New functions

Just call replace the native timing calls with these new VIs. They will call the Native Timing functions unless you happen to create a child class and pass that to Set Timing Source, which will allow you to override any of the timing functions.

## Behind the scenes

This library uses a private global variable to store a DVR of the Native Timing Object. Set Timing Source sets the DVR (which can be a child class) Get Timing Object returns the Native Timing object unless the DVR exists, in which case it returns whatever was in the DVR.

## Safety

To avoid accidentally messing with timing in your built app, the only place that creates the DVR is the Set Timing Source VI. If the DVR is not valid, then it always it always returns the native timing object.
